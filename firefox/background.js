const disallowUrls = [
	{
		includes: "//vk.com/",
		excludes: "https://vk.com",
	},
	{
		includes: "//connect.ok.ru/connect.js",
		excludes: "https://ok.ru"
	},
	{
		includes: "//ok.ru/",
		excludes: "https://ok.ru"
	},
]

const safeIconPath = {64: "safe-icon-64.png"}
const removeIconPath = {64: "remove-icon-red-64.png"}

function setIcon(tabId, iconPath) {
	if(tabId === -1) return

	browser.pageAction.setIcon({path: iconPath, tabId})
	browser.pageAction.show(tabId)
}

let tabBuffer = {}

function watchdog(details) {
	const {url, type, tabId} = details

	if(type === "main_frame") {
		tabBuffer[tabId] = {safe: true}
	}

	for(const item of disallowUrls) {
			if(url.includes(item.includes)
				&& !details.originUrl.startsWith(item.excludes)
				&& type !== "main_frame") {

				setIcon(details.tabId, removeIconPath)
				tabBuffer[tabId] = {...tabBuffer[tabId], safe: false}
				return {cancel: true}
			}
	}

	// if(tabBuffer[tabId] && tabBuffer[tabId].safe) {
	// 	//setIcon(details.tabId, safeIconPath)
	// }
}

browser.webRequest.onBeforeRequest.addListener(
	watchdog,
	{urls: ["<all_urls>"]},
	["blocking"]
)
