#!/bin/bash

if [ ! -d "release" ]; then
  mkdir release
fi

zip release/rwidget_chrome.zip -rj -FS chrome/*
zip release/rwidget_firefox.zip -rj -FS firefox/*
